document.addEventListener("DOMContentLoaded", onLoad);

function onLoad(){
	var list1 = new UserList();
}

function UserList() {
	//создаем запрос
	var xhr = new XMLHttpRequest();
	xhr.open('GET', 'https://api.randomuser.me/1.0/?results=50&nat=gb,us&inc=gender,name,location,email,phone,picture', true);
	xhr.send();
	var object = this;
	xhr.onreadystatechange = function() { 
		//после того, как будет получен ответ от сервера
		if (xhr.readyState != 4) return;
		//проверяем, есть ли в ответе список пользоваталей. Если нет, то выдаем сообщение об ошибке.
		if (xhr.responseText.indexOf("results")==-1){
			alert("Ошибка загрузки данных с сервера, попробуйте перезагрузить страницу.")
			return;
		}
		object.list = JSON.parse(xhr.responseText).results;
		//сортируем и отображаем список на странице, (1) - в алфавитном порядке, (-1) - наоборот.
		object.sortAndRender(1);
	}
	
	this.sortAndRender = function(order){
		
		//сортируем список пользователей в нужном порядке
		object.list.sort(function(a,b){
			var aFull= a.name.first+a.name.last;
			var bFull= b.name.first+b.name.last;
			if (aFull > bFull) return 1*order;
			if (aFull < bFull) return -1*order;
		});
		
		//создаем div, в который будем добавлять div'ы с каждым пользователем
		var userListDiv = document.createElement('div');
		userListDiv.setAttribute('id', 'userList')
		
		//создаем шаблон select'а, который будем копировать для каждого пользователя
		var sortSelect = document.createElement('select');
		sortSelect.className="sortSelect";
		
		//добавляем опции для slect'а
		var azOption = document.createElement('option');
		azOption.innerHTML = 'A-Z';
		azOption.value = 1;
		sortSelect.appendChild(azOption);
		
		var zaOption = document.createElement('option');
		zaOption.innerHTML = 'Z-A';
		zaOption.value = -1;
		sortSelect.appendChild(zaOption);
		
		//сгенерируем div для каждого пользователя
		this.list.forEach(function(user, i){
			var userDiv = document.createElement('div');
			userDiv.className = 'user';
			userDiv.setAttribute('id','user'+i);
			
			var photoDiv = document.createElement('div');
			photoDiv.className = 'photo';
			photoDiv.innerHTML = '<img src="'+user.picture.medium+'">';
			//обработка события для открытия окна с полной информацией
			photoDiv.onclick = function() {object.showFull(i)};
			
			var nameDiv = document.createElement('div');
			nameDiv.className = 'name';
			nameDiv.innerHTML = '<span>'+user.name.title+'. '+user.name.first+' '+user.name.last+'</span>';
			//обработка события для открытия окна с полной информацией
			nameDiv.onclick = function() {object.showFull(i)};
			
			//добавляем блоки с фото и фио в div пользователя
			userDiv.appendChild(photoDiv);
			userDiv.appendChild(nameDiv);
			
			//создаем копию select'a для сортировки
			var newSelect = sortSelect.cloneNode(true);
			//при смене способа сортировки сортируем список заново, согласно выбранному способу и выводим на экран
			newSelect.onchange = function() {object.sortAndRender(newSelect[newSelect.selectedIndex].value)};
			//добавляем сам select  и его контейнер в div пользователя
			var selectDiv = document.createElement('div');
			selectDiv.appendChild(newSelect);
			userDiv.appendChild(selectDiv);
			
			//если выбран обратный порядок, то устанавливаем select в эту позицию
			if (order==-1) 
				newSelect.selectedIndex = 1;
			
			//добавляем div пользователя в общий контейнер
			userListDiv.appendChild(userDiv);
		});
		//после полной генерации списка, заменяем старый список на новый
		document.body.replaceChild(userListDiv, document.getElementById('userList'));
	}
	
	this.showFull = function(id){
		
		//selectedUser хранит номер уже выбранного пользователя
		//если он уже был выбран, то убираем с него класс, задающий подсветку
		if (this.selectedUser!=null) document.getElementById('user'+this.selectedUser).className = 'user';
		//и подсвечиваем текущего пользователя
		this.selectedUser = id;
		document.getElementById('user'+id).className = 'user selected';
		
		var user = this.list[id];
		//заполняем полные данные пользователя в окошке
		document.getElementById('largePhoto').setAttribute('src', user.picture.large);
		document.getElementById('street').innerHTML = 'Street: <span>'+user.location.street+'</span>';
		document.getElementById('city').innerHTML = 'City: <span>'+user.location.city+'</span>';
		document.getElementById('state').innerHTML = 'State: <span>'+user.location.state+'</span>';
		document.getElementById('email').innerHTML = 'E-mail: '+user.email;
		document.getElementById('phone').innerHTML = 'Phone: '+user.phone;
		
		//показываем окошко, т.к. изначально скрыто
		document.getElementById('infoBox').className="visible";
	}
	
}

